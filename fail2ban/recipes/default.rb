#should be run on setup I guess

node[:deploy].each do |application, deploy|

	script "install_fail2ban" do
		interpreter "bash"
		user "root"
		code <<-EOH
			apt-get update
			apt-get install -y fail2ban
		EOH
	end

	# fail2ban jail
	template "/etc/fail2ban/jail.local" do
		source "jail.conf"
	end

	# fail2ban filters
	template "/etc/fail2ban/filter.d/http-access-ddos.conf" do
		source "filter/http-ddos.conf"
	end
	template "/etc/fail2ban/filter.d/apache-xmlrpc.conf" do
		source "filter/apache-xmlrpc.conf"
	end
	template "/etc/fail2ban/filter.d/repeat-offender.conf" do
		source "filter/repeat-offender.conf"
	end

	# fail2ban action
	template "/etc/fail2ban/action.d/repeat-offender.conf" do
		source "action/repeat-offender.conf"
	end

	script "restart_fail2ban" do
		interpreter "bash"
		user "root"
		code "service fail2ban restart"
	end

end
