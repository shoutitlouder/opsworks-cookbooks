node[:deploy].each do |application, deploy|

  script "set_permissions" do
    interpreter "bash"
    user "root"
    cwd "#{deploy[:deploy_to]}/current"
    code <<-EOH
    touch storage/logs/lumen.log
    chmod -R 777 ./admin/storage
    chmod -R 777 ./admin/bootstrap/cache
    EOH
  end

  # write out .env file
  template "#{deploy[:deploy_to]}/current/admin/.env" do
    source 'env.erb'
    mode '0660'
    owner deploy[:user]
    group deploy[:group]
    variables(
      :env => deploy[:environment_variables]
    )
  end

  directory '/etc/apache2/sites-enabled/lively_click_loyalty.conf.d/' do
  owner 'root'
  group 'root'
  mode '0755'
  action :create
end

  template "/etc/apache2/sites-enabled/lively_click_loyalty.conf" do
    source 'apacheconf.erb'
    variables(
      :dir => "#{deploy[:deploy_to]}/current",
      :admin_domain => deploy[:environment_variables]['ADMIN_DOMAIN'],
      :public_domain => deploy[:environment_variables]['PUBLIC_DOMAIN']
    )
  end

  script "update_apache" do
    interpreter "bash"
    user "root"
    code <<-EOH
    service apache2 restart
    EOH
  end

  #update js vars file# write out .env file
  template "#{deploy[:deploy_to]}/current/public/www/js/vars.js" do
    source 'jsvars.erb'
    variables(
      :admin_domain => deploy[:environment_variables]['ADMIN_DOMAIN'],
      :public_domain => deploy[:environment_variables]['PUBLIC_DOMAIN']
    )
  end

  template "#{deploy[:deploy_to]}/current/public/www/auth/vars.php" do
    source 'phpvars.erb'
    variables(
      :admin_domain => deploy[:environment_variables]['ADMIN_DOMAIN'],
      :public_domain => deploy[:environment_variables]['PUBLIC_DOMAIN'],
      :fb_oauth_url => deploy[:environment_variables]['FB_OAUTH_URL']
    )
  end

  cron "add_laravel_cron" do
    command "php #{deploy[:deploy_to]}/current/admin/artisan schedule:run >> /dev/null 2>&1"
    user "root"
  end

end
