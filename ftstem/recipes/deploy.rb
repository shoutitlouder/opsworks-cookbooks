node[:deploy].each do |application, deploy|

  script "set_permissions" do
    interpreter "bash"
    user "root"
    cwd "#{deploy[:deploy_to]}/current"
    code <<-EOH
    touch storage/logs/lumen.log
    touch storage/logs/laravel.log
    mkdir ./storage/tmp
    chmod -R 777 ./storage
    chmod -R 777 ./bootstrap/cache
    chmod -R 777 ./tmp
    chown -R deploy:www-data .
    EOH
  end

  # write out .env file
  template "#{deploy[:deploy_to]}/current/.env" do
    source 'env.erb'
    mode '0660'
    owner deploy[:user]
    group deploy[:group]
    variables(
      :env => deploy[:environment_variables]
    )
  end

  ruby_block "php_ini" do
    block do
      file = Chef::Util::FileEdit.new("/etc/php5/apache2/php.ini")
      file.insert_line_if_no_match("/upload_max_filesize = 2M/", "upload_max_filesize = 5M")
      file.write_file
    end
  end

  service "apache2" do
    action :restart
  end


  cron 'scheduler' do
    command 'php /srv/www/ft_stem/current/artisan schedule:run >> /dev/null 2>&1'
  end


  #template "/etc/apache2/sites-enabled/ftstem.conf" do
#    source 'apacheconf.conf'
  #end

  #script "update_apache" do
#    interpreter "bash"
#    user "root"
#    code <<-EOH
#    service apache2 restart
#    EOH
#  end

end
