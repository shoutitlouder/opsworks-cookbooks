#
# Cookbook Name:: ffsports
# Recipe:: default
#
# Copyright 2016, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#
node[:deploy].each do |application, deploy|

  directory "#{deploy[:deploy_to]}/shared/storage" do
    mode 0775
    owner 'www-data'
    group 'www-data'
    action :create
  end

  directory "#{deploy[:deploy_to]}/shared/storage/framework" do
    mode 0775
    owner 'www-data'
    group 'www-data'
    action :create
  end

  directory "#{deploy[:deploy_to]}/shared/storage/framework/views" do
    mode 0775
    owner 'www-data'
    group 'www-data'
    action :create
  end

  directory "#{deploy[:deploy_to]}/shared/storage/framework/sessions" do
    mode 0775
    owner 'www-data'
    group 'www-data'
    action :create
  end

  directory "#{deploy[:deploy_to]}/shared/storage/exports" do
    mode 0775
    owner 'www-data'
    group 'www-data'
    action :create
  end

  directory "#{deploy[:deploy_to]}/shared/storage/imports" do
    mode 0775
    owner 'www-data'
    group 'www-data'
    action :create
  end

  directory "#{deploy[:deploy_to]}/shared/storage/logs" do
    mode 0775
    owner 'www-data'
    group 'www-data'
    action :create
  end

  script "set_permissions" do
    interpreter "bash"
    user "root"
    cwd "#{deploy[:deploy_to]}/current"
    code <<-EOH
    rm -rf ./storage
    ln -s #{deploy[:deploy_to]}/shared/storage #{deploy[:deploy_to]}/current/storage
    chown -R www-data #{deploy[:deploy_to]}/shared/storage
    EOH
  end

  cron "laravel_cron" do
    command "php #{deploy[:deploy_to]}/current/artisan schedule:run >> /dev/null 2>&1"
  end

  # script "wp_uploads" do
    # interpreter "bash"
    # user "root"
    # cwd "#{deploy[:deploy_to]}/current/wordpress/wp-content"
    # code <<-EOH
    # chmod -R 777 ./uploads
    # chown -R #{deploy[:user]}:#{deploy[:group]} ./uploads
    # EOH
 # end

 #rm -rf #{deploy[:deploy_to]}/current/wordpress/wp-admin
 #rm -rf #{deploy[:deploy_to]}/current/wordpress/wp-includes
 #rm -rf #{deploy[:deploy_to]}/current/wordpress/wp-content
 #ln -s /mnt/ffsmedia/wp-admin #{deploy[:deploy_to]}/current/wordpress/wp-admin
 #ln -s /mnt/ffsmedia/wp-includes #{deploy[:deploy_to]}/current/wordpress/wp-includes
 #ln -s /mnt/ffsmedia/wp-content #{deploy[:deploy_to]}/current/wordpress/wp-content

  # write out .env file
  template "#{deploy[:deploy_to]}/current/.env" do
    source 'env.erb'
    mode '0660'
    owner deploy[:user]
    group deploy[:group]
    variables(
      :env => deploy[:environment_variables]
    )
  end


  #next section is where we do a bunch of updates to the WP structure so it can persist between deploys
  # cp -R #{deploy[:deploy_to]}/current/wordpress/wp-content/themes/ffs #{deploy[:deploy_to]}/current/ffs-theme - Grab the current theme from the repo and put it in a place we can symlink to.  This keeps the theme in the repo respected by the live app
  # cp #{deploy[:deploy_to]}/current/wordpress/wp-config.php /mnt/ffsmedia/wordpress - Grab the current wp-config from the repo and put it to our persistent spot.
  # rm -rf #{deploy[:deploy_to]}/current/wordpress - remove current wordpress dir
  # ln -s /mnt/ffsmedia/wordpress #{deploy[:deploy_to]}/current/wordpress - replace what was our wp dir with our symlink to persistant version
  # cp #{deploy[:deploy_to]}/current/.env /mnt/ffsmedia/ - copy our current .env to the persistent are so wordpress config can see it

  script "symlinks" do
    interpreter "bash"
    user "root"
    code <<-EOH
     cp -R #{deploy[:deploy_to]}/current/wordpress/wp-content/themes/ffs/. /mnt/ffsmedia/wordpress/wp-content/themes/ffs
     cp #{deploy[:deploy_to]}/current/wordpress/wp-config.php /mnt/ffsmedia/wordpress/wp-config.php
     rm -rf #{deploy[:deploy_to]}/current/wordpress
     ln -s /mnt/ffsmedia/wordpress #{deploy[:deploy_to]}/current/wordpress
     cp #{deploy[:deploy_to]}/current/.env /mnt/ffsmedia/
    EOH
  end


  template "/etc/apache2/sites-enabled/first_friends_sports.conf" do
    source 'apacheconf.conf'
  end

  script "php_tags" do
    interpreter "bash"
    user "root"
    code "sed -i 's/short_open_tag = Off/short_open_tag = On/g' /etc/php5/apache2/php.ini"
  end

  script "php_tags" do
    interpreter "bash"
    user "root"
    code "sed -i 's/short_open_tag = Off/short_open_tag = On/g' /etc/php5/apache2/php.ini"
  end

  script "max_upload_size" do
    interpreter "bash"
    user "root"
    code "sed -i 's/upload_max_filesize = 2M/upload_max_filesize = 5M/g' /etc/php5/apache2/php.ini"
  end

  script "update_apache" do
    interpreter "bash"
    user "root"
    code <<-EOH
    service apache2 restart
    EOH
  end

  # script "mv php-parser" do
  #   interpreter "bash"
  #   user "root"
  #   cwd "#{deploy[:deploy_to]}/current/vendor/nikic/php-parser/lib/"
  #   code "mv PHPParser PhpParser"
  # end

  #template "#{deploy[:deploy_to]}/current/wordpress/wp-config.php" do
#    source "wp-config.php"
#  end

end
