node[:deploy].each do |application, deploy|
  script "install_mcrypt" do
    interpreter "bash"
    user "root"
    code <<-EOH
    apt-get install php5-mcrypt
    php5enmod mcrypt
    service apache2 restart
    EOH
  end
end
