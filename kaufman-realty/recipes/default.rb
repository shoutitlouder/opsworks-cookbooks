#
# Cookbook Name:: kaufman-realty
# Recipe:: default
#
# Copyright 2016, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#
node[:deploy].each do |application, deploy|
  if application == "kaufman_realty"
    script "set_permissions" do
      interpreter "bash"
      user "root"
      cwd "#{deploy[:deploy_to]}/current"
      code <<-EOH
      chmod -R 775 ./system
      chmod -R 777 ./assets/cache
      if [ ! -d "./system/cms/cache" ]; then
        mkdir ./system/cms/cache
      fi
      chmod -R 777 ./system/cms/cache
      EOH
    end

    script "memory_limit" do
    	interpreter "bash"
    	user "root"
    	cwd "/etc/php5/apache2/"
    	code <<-EOH
    	sed -i 's/memory_limit = .*/memory_limit = '512M'/' /etc/php5/apache2/php.ini
      sed -i 's/short_open_tag = .*/short_open_tag = On/' /etc/php5/apache2/php.ini
      service apache2 restart
    	EOH
    end
  end
end
