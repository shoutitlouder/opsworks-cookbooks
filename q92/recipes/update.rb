# ONLY DO THIS IF YOU ARE *CERTAIN* YOU WANT TO UPDATE THE wp-content DIRECTORY!
# YOU COULD ALWAYS USE THE MOUNT RECIPE

# RECIPE COMMENTED FOR SAFETY! RE-COMMENT AFTER USE!

script "install_dependencies" do
	interpreter "bash"
	user "root"
	code "sudo apt-get install -y unzip libdigest-hmac-perl"
end
template "/root/.s3curl" do
	source ".s3curl"
	owner "root"
	mode '0600'
end
script "download_wp-content" do
	interpreter "bash"
	user "root"
	code <<-EOH
	cd /mnt/q92wp-content
	chmod 777 ./
	mkdir ./tempdir
	cd tempdir
	curl -o s3-curl.zip http://s3.amazonaws.com/doc/s3-example-code/s3-curl.zip
	unzip s3-curl.zip
	rm s3-curl.zip
	mv s3-curl/s3curl.pl ./s3curl
	chmod +x ./s3curl
	./s3curl --id ben_keys  -- -o wp-content.tar.gz https://s3-us-west-2.amazonaws.com/q92-s3-download/wp-content.tar.gz
	tar xzf wp-content.tar.gz -C /mnt/q92wp-content
	cd ..
	rm -rf tempdir
	cd wp-content
	mv * ..
	cd /
	EOH
end
