node[:deploy].each do |application, deploy|
	script "mount wp-content" do
		interpreter "bash"
			user "root"
			cwd "#{deploy[:deploy_to]}/current"
			code <<-EOH
			mkdir wp-content
			mount /dev/xvdi ./wp-content
			chown -R deploy:www-data wp-content
			chmod -R 0755 wp-content
			EOH
	end
end
