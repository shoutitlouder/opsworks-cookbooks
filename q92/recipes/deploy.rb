#
# Cookbook Name:: ffsports
# Recipe:: default
#
# Copyright 2016, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#
node[:deploy].each do |application, deploy|

  link "#{deploy[:deploy_to]}/current/wp-content" do
    to "/mnt/q92wp-content"
    not_if { ::File.directory?("#{deploy[:deploy_to]}/current/wp-content")}
  end

  script "wp-content_perms" do
    interpreter "bash"
    user "root"
    cwd "#{deploy[:deploy_to]}/current"
    code <<-EOH
    chown -R deploy:www-data wp-content
    chmod -R 0755 wp-content
    EOH
  end

  script "php_tags" do
    interpreter "bash"
    user "root"
    code "sed -i 's/short_open_tag = Off/short_open_tag = On/g' /etc/php5/apache2/php.ini"
  end

  script "update_apache" do
    interpreter "bash"
    user "root"
    code <<-EOH
    service apache2 restart
    EOH
  end

  template "#{deploy[:deploy_to]}/current/wp-config.php" do
    source "wp-config.php"
  end

end
