#
# Cookbook Name:: kaufman-realty
# Recipe:: default
#
# Copyright 2016, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#
node[:deploy].each do |application, deploy|

  if application == "kaufman_realty_2016"

    template "#{deploy[:deploy_to]}/current/listings/.env" do
      source 'env.erb'
      mode '0660'
      owner deploy[:user]
      group deploy[:group]
      variables(
        :env => deploy[:environment_variables]
      )
    end

      script "storage_folder" do
        interpreter "bash"
        user "deploy"
        code <<-EOH
          mkdir -p #{deploy[:deploy_to]}/shared/storage/framework/cache
          mkdir -p #{deploy[:deploy_to]}/shared/storage/framework/sessions
          mkdir -p #{deploy[:deploy_to]}/shared/storage/framework/views
          mkdir -p #{deploy[:deploy_to]}/shared/storage/app
          mkdir -p #{deploy[:deploy_to]}/shared/storage/logs
        EOH
      end

    script "set_permissions" do
      interpreter "bash"
      user "deploy"
      cwd "#{deploy[:deploy_to]}/current/listings"
      code <<-EOH
      rm -rf ./storage
      ln -s #{deploy[:deploy_to]}/shared/storage #{deploy[:deploy_to]}/current/listings/storage
      chmod -R 777 #{deploy[:deploy_to]}/current/listings/bootstrap/cache
      chmod -R 777 #{deploy[:deploy_to]}/shared/storage/logs
      chmod -R 777 #{deploy[:deploy_to]}/shared/storage/app
      chmod -R 777 #{deploy[:deploy_to]}/shared/storage/framework
      EOH
    end


    unless File.exist?('/mnt/kaufman/wordpress/index.php')
      script "initial_wp_copy" do
        interpreter "bash"
        user "root"
        code <<-EOH
          cp -R #{deploy[:deploy_to]}/current/wordpress /mnt/kaufman
        EOH
      end
    end

    unless Dir.exist?('/mnt/kaufman/listings') || File.symlink?('/mnt/kaufman/listings')
      script "listings_sym_link" do
        interpreter "bash"
        user "deploy"
        code <<-EOH
          ln -s /srv/www/kaufman_realty_2016/current/listings /mnt/kaufman/listings
        EOH
      end
    end



    script "symlinks" do
      interpreter "bash"
      user "root"
      code <<-EOH
       cp -R #{deploy[:deploy_to]}/current/wordpress/wp-content/themes/kaufman /mnt/kaufman/wordpress/wp-content/themes
       cp #{deploy[:deploy_to]}/current/wordpress/wp-config.php /mnt/kaufman/wordpress/wp-config.php
       rm -rf #{deploy[:deploy_to]}/current/wordpress
       ln -s /mnt/kaufman/wordpress #{deploy[:deploy_to]}/current/wordpress
       rm -r /mnt/kaufman/wordpress/listings
       ln -s #{deploy[:deploy_to]}/current/listings/public /mnt/kaufman/wordpress/listings
      EOH
    end

    script "update_owner" do
      interpreter "bash"
      user "root"
      code <<-EOH
        chown -R www-data /mnt/kaufman
        chown -h www-data #{deploy[:deploy_to]}/current/wordpress
      EOH
    end

  end

end
