node[:deploy].each do |application, deploy|
  cron "artisan schedule:run" do
    command "cd #{deploy[:deploy_to]}/current/listings && php artisan schedule:run >> /dev/null 2>&1"
  end
end
